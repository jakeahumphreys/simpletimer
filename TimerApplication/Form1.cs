﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace TimerApplication
{
    public partial class Form1 : Form
    {
        private DateTime _targetDateTime;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnStopStart_Click(object sender, EventArgs e)
        {
            if (numHours.Value == 0 && numMinutes.Value == 0 && numSeconds.Value == 0)
                MessageBox.Show("Please populate a value in at least one of the fields.");

            _targetDateTime = DateTime.Now
                .AddHours(Convert.ToDouble(numHours.Value))
                .AddMinutes(Convert.ToDouble(numMinutes.Value))
                .AddSeconds(Convert.ToDouble(numSeconds.Value));

            ToggleFieldsAndButtonState();
            txtLog.AppendText($"Timer will finish: {_targetDateTime.Hour}:{_targetDateTime.Minute}:{_targetDateTime.Second} \n");
            tmrCheckTime.Start();
        }

        private void tmrCheckTime_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (DateTime.Now >= _targetDateTime)
            {
                tmrCheckTime.Stop();
                txtLog.AppendText("Timer reached. \n");
                ToggleFieldsAndButtonState();
            }
        }

        private void ToggleFieldsAndButtonState()
        {
            if (numHours.Enabled)
            {
                numHours.Enabled = false;
                numMinutes.Enabled = false;
                numSeconds.Enabled = false;
                btnStopStart.Text = "Stop";
            }
            else
            {
                numHours.Enabled = true;
                numMinutes.Enabled = true;
                numSeconds.Enabled = true;
                btnStopStart.Text = "Start";
            }
        }
    }
}