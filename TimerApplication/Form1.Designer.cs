﻿namespace TimerApplication
{
    partial class 
        
       Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.numSeconds = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numMinutes = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numHours = new System.Windows.Forms.NumericUpDown();
            this.btnStopStart = new System.Windows.Forms.Button();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.tmrCheckTime = new System.Timers.Timer();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.numSeconds)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.numMinutes)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.numHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.tmrCheckTime)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(239, 113);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Timer Options";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.numSeconds);
            this.groupBox4.Location = new System.Drawing.Point(156, 24);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(67, 51);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Seconds";
            // 
            // numSeconds
            // 
            this.numSeconds.Location = new System.Drawing.Point(6, 19);
            this.numSeconds.Maximum = new decimal(new int[] {60, 0, 0, 0});
            this.numSeconds.Name = "numSeconds";
            this.numSeconds.Size = new System.Drawing.Size(55, 20);
            this.numSeconds.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numMinutes);
            this.groupBox3.Location = new System.Drawing.Point(83, 24);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(67, 51);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Minutes";
            // 
            // numMinutes
            // 
            this.numMinutes.Location = new System.Drawing.Point(6, 19);
            this.numMinutes.Maximum = new decimal(new int[] {60, 0, 0, 0});
            this.numMinutes.Name = "numMinutes";
            this.numMinutes.Size = new System.Drawing.Size(55, 20);
            this.numMinutes.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numHours);
            this.groupBox2.Location = new System.Drawing.Point(10, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(67, 51);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hours";
            // 
            // numHours
            // 
            this.numHours.Location = new System.Drawing.Point(6, 19);
            this.numHours.Name = "numHours";
            this.numHours.Size = new System.Drawing.Size(55, 20);
            this.numHours.TabIndex = 0;
            // 
            // btnStopStart
            // 
            this.btnStopStart.Location = new System.Drawing.Point(12, 133);
            this.btnStopStart.Name = "btnStopStart";
            this.btnStopStart.Size = new System.Drawing.Size(239, 22);
            this.btnStopStart.TabIndex = 1;
            this.btnStopStart.Text = "Start";
            this.btnStopStart.UseVisualStyleBackColor = true;
            this.btnStopStart.Click += new System.EventHandler(this.btnStopStart_Click);
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(15, 162);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(236, 115);
            this.txtLog.TabIndex = 2;
            // 
            // tmrCheckTime
            // 
            this.tmrCheckTime.Interval = 1000D;
            this.tmrCheckTime.SynchronizingObject = this;
            this.tmrCheckTime.Elapsed += new System.Timers.ElapsedEventHandler(this.tmrCheckTime_Elapsed);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 289);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.btnStopStart);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Simple Timer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.numSeconds)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.numMinutes)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.numHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.tmrCheckTime)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown numHours;
        private System.Windows.Forms.NumericUpDown numMinutes;
        private System.Windows.Forms.NumericUpDown numSeconds;
        private System.Timers.Timer tmrCheckTime;

        private System.Windows.Forms.GroupBox groupBox2;

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnStopStart;
        private System.Windows.Forms.TextBox txtLog;

        #endregion
    }
}